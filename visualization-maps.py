import numpy as np  # useful for many scientific computing in Python
import pandas as pd # primary data structure library

import folium

# define the world map
world_map = folium.Map()

# display world map
world_map

# open map in browser
import os
import webbrowser
filepath = 'map.html'

world_map = folium.Map()
world_map.save(filepath)

webbrowser.open('file://' + filepath)

#
# define the world map centered around Canada with a low zoom level
world_map = folium.Map(location=[56.130, -106.35], zoom_start=4)

# display world map
world_map



