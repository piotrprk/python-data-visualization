import numpy as np  # useful for many scientific computing in Python
import pandas as pd # primary data structure library

# import data
df_can = pd.read_excel('https://s3-api.us-geo.objectstorage.softlayer.net/cf-courses-data/CognitiveClass/DV0101EN/labs/Data_Files/Canada.xlsx',
                       sheet_name='Canada by Citizenship',
                       skiprows=range(20),
                       skipfooter=2)

print ('Data read into a pandas dataframe!')

df_can.info()

# size of dataframe
df_can.shape

# drop unnecessary columns
# in pandas axis=0 represents rows (default) and axis=1 represents columns.
df_can.drop(['AREA','REG','DEV','Type','Coverage'], axis=1, inplace=True)
df_can.head(2)

# rename some columns
df_can.rename(columns={'OdName':'Country', 'AreaName':'Continent', 'RegName':'Region'}, inplace=True)
df_can.columns

# sum up and add 'Total' column
df_can['Total'] = df_can.sum(axis=1)
df_can[['Country', 'Total']]

# check for null values
df_can.isnull().sum()

# look at the data
df_can.describe()

# set country as the index column
df_can.set_index('Country', inplace=True)
# tip: The opposite of set is reset. So to reset the index, we can use df_can.reset_index()
# optional: to remove the name of the index
df_can.index.name = None

# select Japan
# 3. for years 1980 to 1985
print(df_can.loc['Japan', [1980, 1981, 1982, 1983, 1984, 1984]])
print(df_can.iloc[87, [3, 4, 5, 6, 7, 8]])

# change all columns type to str
df_can.columns = list(map(str, df_can.columns))
print ( [type(x) for x in df_can.columns.values])

# useful for plotting later on
years = list(map(str, range(1980, 2014)))

# filter data

# 1. create the condition boolean series
condition = df_can['Continent'] == 'Asia'
print(condition)
df_can[condition]

# we can pass mutliple criteria in the same line.
# let's filter for AreaNAme = Asia and RegName = Southern Asia

df_can[(df_can['Continent']=='Asia') & (df_can['Region']=='Southern Asia')]


# save data
df_can.to_csv('canada-immigration.csv', index=True, index_label='Country')



