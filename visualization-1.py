import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

# change matplot style !
print(plt.style.available)
mpl.style.use(['ggplot']) # optional: for ggplot-like style

# load dataframe

df_can = pd.read_csv('canada-immigration.csv')
# set country as the index column
df_can.set_index('Country', inplace=True)
df_can.index.name = None

# helper variable
years = list(map(str, range(1980, 2014)))

# plot in Pandas
haiti = df_can.loc['Haiti', years] # passing in years 1980 - 2013 to exclude the 'total' column
haiti.head()

haiti.plot()

# extend
haiti.index = haiti.index.map(int) # let's change the index values of Haiti to type integer for plotting
haiti.plot(kind='line')

plt.title('Immigration from Haiti')
plt.ylabel('Number of immigrants')
plt.xlabel('Years')
# annotate the 2010 Earthquake.
# syntax: plt.text(x, y, label)
plt.text(2000, 6000, '2010 Earthquake') # see note below
plt.show()

# China and India
china_india = df_can.loc[['China', 'India'], years]

# transpose dataframe
china_india = china_india.transpose()

china_india.index = china_india.index.map(int)
china_india.plot(kind='line')
plt.title('Immigration from China and India')
plt.ylabel('Number of immigrants')
plt.xlabel('Years')
plt.show()

# top 5 by total

top_5 = df_can.sort_values('Total', ascending=False)[:5][years].transpose()
top_5.index = top_5.index.map(int)

top_5.plot(kind='line')
plt.title('Top 5 immigration to Canada')
plt.ylabel('Number of immigrants')
plt.xlabel('Years')
plt.show()




