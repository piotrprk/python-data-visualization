import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.style.use('ggplot') # optional: for ggplot-like style

# load dataframe
df_can = pd.read_csv('canada-immigration.csv')
# set country as the index column
df_can.set_index('Country', inplace=True)
df_can.index.name = None

# helper var
years = list(map(str, range(1980, 2014)))

# top 5
top_5 = df_can.sort_values(['Total'], ascending=False, axis=0).head()

# transpose
top_5 = top_5[years].transpose()

# Area Plot

# change index to int for ploting
top_5.index = top_5.index.map(int)

# plot
top_5.plot(kind='area', stacked=False, alpha=.25, figsize=(20, 10))
plt.title('Immigration Trend of Top 5 Countries')
plt.ylabel('Number of Immigrants')
plt.xlabel('Years')

plt.show()

# working on Artist layer
ax = top_5.plot(kind='area', alpha=0.35, figsize=(20, 10))

ax.set_title('Immigration Trend of Top 5 Countries')
ax.set_ylabel('Number of Immigrants')
ax.set_xlabel('Years')

# least 5 stacked area plot
tail_5 = df_can.sort_values(['Total'], ascending=False, axis=0).tail()
tail_5 = tail_5[years].transpose()
tail_5.index = tail_5.index.map(int)
tail_5.plot(kind='area', alpha=0.45, figsize=(20, 10))
plt.title('Immigration trend of least 5 countries')
plt.ylabel('Number of immigrants')
plt.xlabel('Years')
plt.show()

# with artist layer
tail_5.index = tail_5.index.map(int)
ax = tail_5.plot(kind='area', alpha=0.55, figsize=(20, 10))

ax.set_title('Immigration Trend of LEast 5 Countries')
ax.set_ylabel('Number of Immigrants')
ax.set_xlabel('Years')

# Histogram

# split data into intervals/bin with numpy
count, bin_edges = np.histogram(df_can['2013']) # 10 bins by default
print(count, bin_edges)

# plot
df_can['2013'].plot(kind='hist', figsize=(8,5), xticks=bin_edges) # xticks fixes x-axis

plt.title('Histogram of Immigration from 195 Countries in 2013') # add a title to the histogram
plt.ylabel('Number of Countries') # add y-label
plt.xlabel('Number of Immigrants') # add x-label

plt.show()

# multiple histograms on one plot
# df_can.loc[['Denmark', 'Norway', 'Sweden'], years].plot.hist()
df_3 = df_can.loc[['Denmark', 'Norway', 'Sweden'], years].transpose()
# let's get the x-tick values
count, bin_edges = np.histogram(df_3, 15)

df_3.plot(kind='hist', figsize=(10,6),
          bins = 15, # increase bins number
          alpha = 0.6,
          xticks = bin_edges,
          color = ['coral', 'darkslateblue', 'mediumseagreen'])
plt.title('Histogram of Immigration from Denmark, Norway, and Sweden from 1980 - 2013')
plt.ylabel('Number of Years')
plt.xlabel('Number of Immigrants')
plt.show()

# stack plots
count, bin_edges = np.histogram(df_3, 15)
xmin = bin_edges[0] - 10   #  first bin value is 31.0, adding buffer of 10 for aesthetic purposes
xmax = bin_edges[-1] + 10  #  last bin value is 308.0, adding buffer of 10 for aesthetic purposes

df_3.plot(kind='hist', figsize=(10,6),
          bins = 15, # increase bins number
          alpha = 0.6,
          xticks = bin_edges,
          color = ['coral', 'darkslateblue', 'mediumseagreen'],
          stacked = True,
          xlim = (xmin, xmax))
plt.title('Histogram of Immigration from Denmark, Norway, and Sweden from 1980 - 2013')
plt.ylabel('Number of Years')
plt.xlabel('Number of Immigrants')
plt.show()

# histogram for Greece Albania and Bulgaria
df_3 = df_can.loc[['Greece', 'Albania', 'Bulgaria'], years].transpose()
count, bin_edges = np.histogram(df_3, 15)

df_3.plot(kind='hist', figsize=(10,6),
          bins = 15,
          alpha = 0.35,
          xticks = bin_edges
          )
plt.title('Histogram of Immigration from Denmark, Norway, and Sweden from 1980 - 2013')
plt.ylabel('Number of Years')
plt.xlabel('Number of Immigrants')
plt.show()

# BAR charts

# step 1: get the data
df_iceland = df_can.loc['Iceland', years]
# step 2: plot data
df_iceland.plot(kind='bar', figsize=(10, 6))

plt.xlabel('Year') # add to x-label to the plot
plt.ylabel('Number of immigrants') # add y-label to the plot
plt.title('Icelandic immigrants to Canada from 1980 to 2013') # add title to the plot

plt.show()

# annotate on a graph
df_iceland.plot(kind='bar', figsize=(10, 6), rot=90)

plt.xlabel('Year')
plt.ylabel('Number of Immigrants')
plt.title('Icelandic Immigrants to Canada from 1980 to 2013')

# Annotate arrow
plt.annotate('',                      # s: str. will leave it blank for no text
             xy=(32, 70),             # place head of the arrow at point (year 2012 , pop 70)
             xytext=(28, 20),         # place base of the arrow at point (year 2008 , pop 20)
             xycoords='data',         # will use the coordinate system of the object being annotated
             arrowprops=dict(arrowstyle='->', connectionstyle='arc3', color='blue', lw=2)
            )

# Annotate Text
plt.annotate('2008 - 2011 Financial Crisis', # text to display
             xy=(28, 30),                    # start the text at at point (year 2008 , pop 30)
             rotation=72.5,                  # based on trial and error to match the arrow
             va='bottom',                    # want the text to be vertically 'bottom' aligned
             ha='left',                      # want the text to be horizontally 'left' algned.
            )

plt.show()

# top 15 countries on horizontal graph

top_15 = df_can.sort_values(['Total'], ascending=False)['Total'].head(15)
top_15.transpose().plot(kind='barh', figsize=(10,6))
plt.xlabel('Number of Immigrants')
plt.title('Top 15 Conuntries Contributing to the Immigration to Canada between 1980 - 2013')
# annotate value labels to each country
for index, value in enumerate(top_15):
    label = "{:,}".format(value)  # format thousands with commas
    # place text at the end of bar (subtracting 47000 from x, and 0.1 from y to make it fit within the bar)
    plt.annotate(label, xy=(value - 50000, index - 0.15), color='white')
plt.show()


# full list of colors in Matplotlib
import matplotlib
for name, hex in matplotlib.colors.cnames.items():
    print(name, hex)

