import numpy as np
import pandas as pd

import matplotlib as mpl
import matplotlib.pyplot as plt

mpl.style.use('ggplot') # optional: for ggplot-like style

# load dataframe
df_can = pd.read_csv('canada-immigration.csv')
# set country as the index column
df_can.set_index('Country', inplace=True)
df_can.index.name = None

# helper var
years = list(map(str, range(1980, 2014)))


# Seaborn
# import library
import seaborn as sns

# get total and years DF
# we can use the sum() method to get the total population per year
df_tot = pd.DataFrame(df_can[years].sum(axis=0))

# change the years to type float (useful for regression later on)
df_tot.index = map(float, df_tot.index)

# reset the index to put in back in as a column in the df_tot dataframe
df_tot.reset_index(inplace=True)

# rename columns
df_tot.columns = ['year', 'total']

# view the final dataframe
df_tot.head()

# reg plot
plt.figure(figsize=(15, 10))
sns.set(font_scale=1.5) # font size
sns.set_style('ticks') # change background to white background

ax = sns.regplot(x='year', y='total',
                 data=df_tot,
                 marker='+',
                 scatter_kws={'s': 200})
ax.set(xlabel='Year', ylabel='Total Immigration') # add x- and y-labels
ax.set_title('Total Immigration to Canada from 1980 - 2013') # add title
